/**
 * Le programme pour détecter les nombres narcissiques.
 * 
 * Pour compiler et exécuter le programme sur un système UNIX, il faut taper les lignes de commande suivantes :
 * 		gcc exercice1.c -lm -o exo1   # pour compiler.
 * 		./exo1                        # pour exécuter.
 *
 * @created 2020/03/20
 * @updated 2020/30/20
 *
 */


# include <stdlib.h>
# include <stdio.h>

/**
 * On dois aussi inclure le header <math.h> :
 *  + Pour utiliser la fonction pow( x, y ) pour le calcul de x exposant y.
 *  + Pour utiliser la fonction floor( x ) pour la partie entière de x.
 *
 * On peu aussi utiliser (int)x pour avoir la partie entière de x.
 */
# include <math.h>

/**
 * La fonction principale de notre programme appelée 'main'.
 * Elle est unique et doit toujours se figurer dans tous programme.
 * Si cette fonction est manquante, alors le compilateur ne génère
 * pas le fichier exécutable de votre programme.
 * 
 */
int main( void ) {
	/**
	 * On déclare et on initialise à 0, les deux variables n1 et n2.
	 * Ces deux variables représentent l'intervalle de recherche
	 * des nombres narcissiques.
	 */
	int n1 = 0;
	int n2 = 0;

	/**
	 * Etant donné que la définition de la fonction :
	 * afficher_nombres_narcissiques( a, b )
	 * se trouve après celle de la fonction main( void ), alors
	 * on doit la déclarer ici comme une variable pour 
	 * pouvoir l'utiliser.
	 */
	void afficher_nombres_narcissiques( int a, int b );

	// On affiche un message pour demander à l'utilisateur de saisir
	// deux nombres entiers naturels non nules.
	printf( "Entrer deux nombres entiers naturels non nuls.\n" );
	/**
	 * On appel la fonction qui permet de récupérer les deux nombres
	 * entiers saisi au clavier.
	 * Ces nombres seront dans l'ordre saisis dans n1 ensuite dans n2.
	 */
R:	scanf( "%d %d", &n1, &n2 );
	
	// On vérifie les entrés de l'utilisateur ( Never trust user input )
	// Toujour faire cela quand il s'agit des données entrées par un utilisateur.
	// Si n1 et n2 ne respectent pas les conditions requises
	if ( n1 <= 0 || n2 <= 0 ) {
		// On affiche un message dit message d'erreur.
		printf( "Erreur de saissi.\nLes nombres entres doivent etres des entiers naturels non nuls.\nReessayez svp !\n" );
		// On retourne à la ligne d'étiquette R.
		// C'est la où on récupère les deux nombres saisis par l'utilisateur.
		goto R;
	}

	// On saute une ligne.
	printf( "\n" );
	// On appel en fin la fonction du siècle !! ;-)
	afficher_nombres_narcissiques( n1, n2 );

	return EXIT_SUCCESS; // Vous savez quoi ...

}

/**
 * La fonction qui permet d'obtenir le k-ème chiffre
 * d'un nombre entier naturel.
 *
 * @params position entier { 0 -> n }, nombre entier { n > 0 } 
 * @return un entier
 *
 */
int keme_nombre( int k, int x ) {
	int x_k = floor( x / pow( 10, k) ) - 10 * floor( x / pow( 10, k + 1 ) );
	return x_k;

	/**
	 * On peux aussi faire directement comme suite :
	 * 		return floor( x / pow( 10, k) ) - 10 * floor( x / pow( 10, k + 1 ) );
	 *
	 * Pour reduire le nombre d'oppération.
	 *
	 */

}

/**
 * La fonction qui permet d'obtenir le nombre de chiffres
 * que compose un nombre x > 0.
 *
 * @params nombre entier { n > 0 }
 * @return un entier
 *
 */
int nombre_de_chiffres( int x ) {
	// On a p <- 0. On suppose que x contient 0 chiffre.
	int p = 0;

	// Tant que E(x) est différent de 0, on fait les oppérations suivantes :
	while ( floor( x ) != 0 ) {
		// On divise x par 10;
		x /= 10;
		// On ajoute 1 à p;
		p ++;
	}

	// Lorsque E(x) est maintenant égale à 0, on retourne le nombre p de chiffres.
	return p;

}

/**
 * La fonction qui permet de détecter un nombre narcissique.
 *
 * @params nombre entier { n > 0 }
 * @return un booléen ( int )
 *
 */
int nombre_narcissique( int x ) {
	// On stock dans p le nombre de chiffres de x.
	int p = nombre_de_chiffres( x );
	// On déclare et on intitialise à 0, un compteur k pour la boucle 'for'
	// qui va nous permettre de faire la somme de façon successive.
	int k = 0;
	// On déclare et on initialise la variable 'somme' à 0.
	// C'est dans cette variable qu'on fera la somme successivement.
	int somme = 0;

	// Pour k allant de 0 à p - 1 on fait :
	for ( ; k < p; k++ )
		// La somme des xk exposant p.
		somme += pow ( keme_nombre( k, x ), p );

	// A la fin de la somme, on compare et on retourne directement, 
	// le résultat booléen de la comparaison de la valeur de 'somme' à celle de 'x'. 
	return somme == x;

	/**
	 * On pouvait aussi remplaçer la ligne [152] par le code suivant :
	 * 
	 * 152. if ( somme == x )
	 * 153.     return 1;
	 * 154. else
	 * 155.     return 0;
	 * 156.
	 *
	 */

}

/**
 * La fonction qui permet d'afficher tous les nombres narcissiques compris
 * entre deux nombres entiers naturels non nuls.
 *
 * @params nombre entier { n > 0 }, nombre entier { n > 0 }
 * @return void
 *
 */
void afficher_nombres_narcissiques( int a, int b ) {
	/**
	 * On prévois le cas où 'b' est inférieur à 'a'
	 * et on échange les valeurs respectives de
	 * ces deux variables en se servant d'une
	 * troisième variable 'c'.
	 */
	if ( b < a ) {
		int c = b;
		b = a;
		a = c;
	}

	int n = a;

	// Pour n allant de a à b, on fait :
	for ( ; n <= b; n ++ )
		// Afficher la valeur de n, si la fonction nombre_narcissique( n ) retourne 1.
		if ( nombre_narcissique( n ) )
			printf( "\t%d\n", n );

	// On retourne rien.
	// PS : Cette instruction est facultative, donc on peux la supprimer du code.
	return ;

}