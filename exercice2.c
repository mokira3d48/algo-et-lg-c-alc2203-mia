/**
 * Implémentation des nombres complexes
 * ------------------------------------
 *
 * On va écrire un programme qui va permettre de calculer la somme de deux nombres complexes, 
 * le produit de deux nombres complexes, et le module d'un nombre complexe.
 *
 * Pour compiler et exécuter le programme sur un système UNIX, il faut taper les lignes de commande suivantes :
 * 		gcc exercice2.c -lm -o exo2   # pour compiler.
 * 		./exo2                        # pour exécuter.
 *
 * @created 2020/03/23
 * @updated 2020/04/16
 *
 */


# include <stdlib.h>
# include <stdio.h>

/**
 * On dois aussi inclure le header <math.h> :
 *  + Pour utiliser la fonction sqrt( x ) pour le calcul de la racine carrée de x.
 */
# include <math.h>

/**
 * Structure d'un nombre complexe.
 */
struct complex_t {
	double re;
	double im;

};

/**
 * Après la ligne [21], pour déclarer un nombre complexe, 
 * on le fait comme suite : 
 * struct complex_t z;
 *
 * Trop long n'est ce pas ?? Donc va simplifier cela en écrivant
 * la ligne de code suivante.
 */
typedef struct complex_t Complex;

/**
 * On choisir de déclarer toutes les fonctions en globale.
 * Dans un fichier source (.c), lorsqu'on déclare une fonction en globale, 
 * elle peut être appelée n'importe où en dessous dans ce fichier.
 */
void printComplex( const Complex z );
Complex addComplex( const Complex z1, const Complex z2 );
Complex prodComplex( const Complex z1, const Complex z2 );
double absComplex( const Complex z );


/**
 * La fonction principale de notre programme appelée 'main'.
 * Elle est unique et doit toujours se figurer dans tous programme.
 * Si cette fonction est manquante, alors le compilateur ne génère
 * pas le fichier exécutable de votre programme.
 * 
 */
int main( void ) {
	// On a besoin d'une variable pour gérer les options.
	// On l'initialise à 0 ('\0').
	char option = '\0';

	// On déclare deux variables de type Complex.
	Complex z1 = { 0.0, 0.0 };
	Complex z2 = { 0.0, 0.0 };

DEBUT :
	// On efface la console.
	# if defined( WIN32 ) || defined( WIN64 )
		// On utilise la commande 'cls' pour effacer
		// la console sous windows.
		system( "cls" );
	# else
		// On utilise la commande 'clear' pour effacer
		// la console sous un système UNIX.
		system( "clear" );
	# endif
	
	printf( "#####################################################################\n" );
	printf( "#####################################################################\n" );
	printf( "@#                                                                 @#\n" );
	printf( "@#  Tapez '1' : pour additionner deux nombres complexes ;          @#\n" );
	printf( "@#  Tapez '2' : pour faire le produit de deux nombres complexes ;  @#\n" );
	printf( "@#  Tapez '3' : pour calculer le module d'un nombre complexe;      @#\n" );
	printf( "@#  Tapez '0' : pour sortir du programme.                          @#\n" );
	printf( "@#                                                                 @#\n" );
	printf( "#####################################################################\n" );
	printf( "#####################################################################\n" );
	printf( "\n>_  " );

	scanf( "%c", &option );
//	fflush( stdin );

	printf("\n");

	switch ( option ) {
		case '1' : 
			printf( "Entrer les deux nombres complexes a additionner.\n" );
			printf( "z1 = " );
			scanf( "%lf %lf", &z1.re, &z1.im );
			printf( "z2 = " );
			scanf( "%lf %lf", &z2.re, &z2.im );

			printf( "( " );
			printComplex( z1 );
			printf( " ) + ( " );
			printComplex( z2 );
			printf( " )" );
			printf( " = " );
			printComplex( addComplex( z1, z2 ) );
			printf( "\n" );
			break;

		case '2' : 
			printf( "Entrer les deux nombres complexes dont on veux faire le produit.\n" );
			printf( "z1 = " );
			scanf( "%lf %lf", &z1.re, &z1.im );
			printf( "z2 = " );
			scanf( "%lf %lf", &z2.re, &z2.im );

			printf( "( " );
			printComplex( z1 );
			printf( " ) ( " );
			printComplex( z2 );
			printf( " )" );
			printf( " = " );
			printComplex( prodComplex( z1, z2 ) );
			printf( "\n" );
			break;

		case '3' : 
			printf( "Entrer un nombre complexe.\n" );
			printf( "z = " );
			scanf( "%lf %lf", &z1.re, &z1.im );

			printf( "| " );
			printComplex( z1 );
			printf( " |" );
			printf( " = %f\n", absComplex( z1 ) );
			break;

		case '0' : return EXIT_SUCCESS;
	}

	printf( "\nAppuyez sur touche [Enter] pour continuer..." );
	getchar();
	getchar();

	goto DEBUT;

 	return EXIT_SUCCESS;

}

/**
 * Fonction d'affichage d'un nombre complexe.
 *
 * @params un nombre Complex
 * @return void
 */
void printComplex( const Complex z ) {
	if ( z.re != 0.0 ) {
		printf( "%.3f", z.re );
	
		if ( z.im < 0.0 )
			printf( " - %.3fi", -1 * z.im );
		else if ( z.im > 0.0 )
			printf( " + %.3fi", z.im );

	}
	else {
		if ( z.im != 0.0 )
			printf( "%.3fi", z.im );
		else
			printf( "0.0" );
	}

	// L'instruction return est facultative dans cette fonction.
	// La fonction ne retourne rien.
	return ;

}

/**
 * Fonction qui permet de calculer la somme de deux nombres complexes.
 *
 * @params deux nombres Complex z1 et z2
 * @return un nombre Complex
 */
Complex addComplex( const Complex z1, const Complex z2 ) {
	Complex z3 = { z1.re + z2.re, z1.im + z2.im };
	return z3;

}

/**
 * Fonction qui permet de calculer la produit de deux nombres complexes.
 *
 * @params deux nombres Complex z1 et z2
 * @return un nombre Complex
 */
Complex prodComplex( const Complex z1, const Complex z2 ) {
	// On va faire simple en utilisant 2 variables.
	double x1 = z1.re * z2.re - z1.im * z2.im;   // Je sais que vous avez compris. i x i = -1.
	double x2 = z1.re * z2.im + z1.im * z2.re;   // On est d'accord !

	Complex z3 = { x1, x2 };

	return z3;

}

/**
 * Fonction qui permet de calculer le module d'un nombre complexe.
 *
 * @params un nombre Complex
 * @return un nombre réel double
 */
double absComplex( const Complex z ) {
	return sqrt( z.re * z.re + z.im * z.im );
	// x * x = x².

}


